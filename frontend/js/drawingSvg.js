/**
 * Created by Виталий on 26.06.2017.
 */
import anime from 'animejs';
export default class DrawingSvg {
    constructor(selector, duration = '1000') {
        DrawingSvg.dashSet(selector);
        return anime({
            targets: selector,
            easing: 'linear',
            duration: duration,
            strokeDashoffset: [anime.setDashoffset, 0],
            autoplay: false
        });
    }
    static dashSet(selector) {
        document.querySelectorAll(selector)
            .forEach(el => DrawingSvg.strokeDash(el));
    }
    static strokeDash(el) {
        const pathLength = el.getTotalLength();
        el.style.strokeDashoffset = pathLength;
    }
}