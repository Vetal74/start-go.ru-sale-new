/**
 * Created by Виталий on 26.06.2017.
 */
//Класс, реализующий загрузку ресурсов на страницу
export default class ResLoader {
    constructor() {}
    static loadStyles(style, directoryStyle = 'dist/css/', dotcss = '.css') {
        let link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = directoryStyle + style + dotcss;
        document.querySelector('head').appendChild(link);
    }
    static loadImages(img = 'img.lazy-load', div = 'div.lazy-load') {
        $(img).each((key, val) => {
            let src = $(val).data('src');
            $(val).attr('src', src);
        });
        $(div).each((key, val) => {
            $(val).addClass('load');
        });
    }
}