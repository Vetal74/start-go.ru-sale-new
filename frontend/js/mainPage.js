/**
 * Created by Виталий on 26.06.2017.
 */
import anime from 'animejs';
import ResLoader from './loader';
import DrawingSvg from './drawingSvg';
export default class MainPage {
    constructor(img) {
        this.img = document.createElement('img');
        this.img.src = img;
        this.prevElem = 1;

        this.block2Animate = this.onScroll();
    }
    onLoad() {
        const self = this;
        return new Promise((resolve) => {
            $(self.img).on('load', () => {
                self.start();
                self.loadStyles();
                self.owlLoad();
                self.loadBitrix();
                setTimeout(resolve, 500);
            });
        });
    }
    start(timeout = 300) {
        setTimeout(() => {
            $('.block-1 .nbook').addClass('loaded');
        }, timeout);
    }
    loadStyles() {
        ResLoader.loadStyles('style');
        ResLoader.loadStyles('owl.carousel.min', 'src/owl/');
        ResLoader.loadStyles('owl.theme.default.min', 'src/owl/');
        ResLoader.loadStyles('jquery.fancybox', 'src/');
        ResLoader.loadImages();
    }
    owlLoad() {
        import('owl.carousel')
            .then(res => {
                this.owlInit();
                this.dotBlob();
            })
            .catch(err => {
                new Error(err);
            });
    }
    loadBitrix() {
        (function(w,d,u,b){
            let s=d.createElement('script');
            let r=(Date.now()/1000|0);
            s.async=1;s.src=u+'?'+r;
            let h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b2191857/crm/site_button/loader_2_pnvxx1.js');
        let $body = $('body');
        $body.on('click', '.b-call', function() {
            $('.b24-widget-button-callback')[0].click();
            return false;
        });
        $body.on('click', '.b-chat', function() {
            $('.b24-widget-button-openline_livechat')[0].click();
            return false;
        });
    }
    dotBlob(elem = 0) {
        if(elem === this.prevElem) return false;
        this.prevElem = elem;
        let dist = $('.slider-navigation .dot').eq(elem).position().left;
        let duration = 600;
        let translateAnimate = anime({
            targets: '.slider-navigation .select',
            duration: duration,
            translateX: dist,
            scaleX: [
                {
                    value: 1.55,
                    duration: (duration / 2)
                }, {
                    value: 1,
                    duration: (duration / 2),
                    delay: (duration / 1024),
                    easing: 'easeOutBack'
                }
            ],
            scaleY: [
                {
                    value: 0.65,
                    duration: (duration / 2)
                }, {
                    value: 1,
                    duration: (duration / 2),
                    delay: (duration / 1024),
                    easing: 'easeOutBack'
                }
            ],
            easing: 'easeOutBack'
        });
    }
    owlInit() {
        let owl = $('.block-3 .slider .carousel'),
            owlPartners = $('.block-5 .banners.owl-carousel');
        owl.owlCarousel({
            items: 2,
            slideBy: 2,
            loop: true,
            dotsContainer: '.slider-navigation .dots',
            dotClass: 'dot',
            dotsEach: false,
            responsive: {
                0: {
                    items: 1
                },
                920: {
                    items: 2
                }
            },
            onTranslate: (e) => {
                this.dotBlob(e.page.index);
            }
        });
        let nav = $('.block-3 .slider-navigation'),
            navPartners = $('.block-5 .arrows');
        nav.on('click', '.nav.back', () => {
            owl.trigger('prev.owl.carousel');
        });
        nav.on('click', '.nav.next', () => {
            owl.trigger('next.owl.carousel');
        });

        owlPartners.owlCarousel({
            items: 6,
            loop: true,
            responsive: {
                0: {
                    items: 2
                },
                400: {
                    items: 3
                },
                600: {
                    items: 4
                },
                800: {
                    items: 5
                },
                1000: {
                    items: 6
                }
            }
        });
        navPartners.on('click', '.prev', () => {
            owlPartners.trigger('prev.owl.carousel');
        });
        navPartners.on('click', '.next', () => {
            owlPartners.trigger('next.owl.carousel');
        });
    }
    onScroll() {
        let block2Animate = new DrawingSvg('.block-2 path', '1500'),
            block4Animate = new DrawingSvg('.block-4 path', '1500');
        let tumblerBlock2 = true;
        $(window).on('scroll', function() {
            let $this = $(this),
                scrollTop = $this.scrollTop();
            if(scrollTop > screen.height / 3.5 && tumblerBlock2) {
                $('.block-2 .icon').addClass('active');
                setTimeout(() => {
                    block2Animate.play();
                }, 350);
                tumblerBlock2 = false;
            }
            if(scrollTop > $('.block-4').offset().top - screen.height / 2.5) {
                block4Animate.play();
                $(window).off('scroll');
            }
        });
        return block2Animate;
    }
}