/**
 * Created by Виталий on 26.06.2017.
 */
export default class MaskInput {
    constructor() {
        this.inputmask = 0;
    }
    static loadMask(loaded) {
        import('inputmask')
            .then(res => {
                this.inputmask = res;
                loaded();
            })
            .catch(e => new Error(e));
    }
    static initMask(Inputmask = this.inputmask) {
        Inputmask({
            "mask": '+7 (999) 999-99-99'
        }).mask('input[type=tel]');
    }
}