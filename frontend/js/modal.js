/**
 * Created by Виталий on 26.06.2017.
 */
import MaskInput from './maskInput';
export default class Modal {
    constructor(props = 0) {
        this.modalProps = {
            globalClass: props.globalClass || 'modal',
            modalClass: props.modalClass || 'modal',
            activeClass: props.activeClass || 'active',
            animateItemsClass: props.animateItemsClass || 'animate',
            layoutClass: props.layoutClass || 'layout-modal'
        };
    }
    open() {
        $(`.${this.modalProps.globalClass}`).removeClass(this.modalProps.activeClass);
        let thisModal = $(`.${this.modalProps.globalClass}.${this.modalProps.modalClass}`);
        this.init(thisModal);
        thisModal.addClass(this.modalProps.activeClass);
        thisModal.find(`.${this.modalProps.animateItemsClass}`).addClass(this.modalProps.activeClass);
        $(`.${this.modalProps.layoutClass}`).addClass(this.modalProps.activeClass);
        setTimeout(() => {
            MaskInput.loadMask(MaskInput.initMask);
        }, 500);
    }
    static close() {
        Modal.iniVars();
        $(`.${Modal.modalProps.layoutClass}, .${Modal.modalProps.globalClass}, .${Modal.modalProps.globalClass} .${Modal.modalProps.animateItemsClass}`).removeClass(Modal.modalProps.activeClass);
    }
    init(thisModal) {
        thisModal.css('height', this.getSize(thisModal));
    }
    getSize(thisModal) {
        let needHeight = 140;
        needHeight += thisModal.find('>.wrapper').outerHeight(true);
        return needHeight;
    }
    static iniVars() {
        Modal.modalProps = {
            globalClass: 'modal',
            modalClass: 'modal',
            activeClass: 'active',
            animateItemsClass: 'animate',
            layoutClass: 'layout-modal'
        };
    }
}