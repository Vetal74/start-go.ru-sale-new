/**
 * Created by Виталий on 29.06.2017.
 */
export default (() => {
    let consoleImage = function(url, scale) {
        scale = scale || 1;
        let img = new Image();
        function getBox(width, height) {
            return {
                string: "+",
                style: "font-size: 1px; padding: " + Math.floor(height/2) + "px " + Math.floor(width/2) + "px; line-height: " + height + "px;"
            }
        }
        img.onload = function() {
            let dim = getBox(this.width * scale, this.height * scale);
            console.log(`%c${dim.string}`, dim.style + `background-image: url(${url}); background-size: ${this.width * scale}px ${this.height * scale}px; color: transparent;`);
        };

        img.src = url;
    };
    consoleImage('http://start-go.pro/vetal/fry.jpg', 0.5);
    console.log("%c   vetal@lapastilla.ru    ", "background-color: #ffdd2d; color: red;");
})();