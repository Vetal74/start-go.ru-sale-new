/**
 * Created by Виталий on 07.06.2017.
 */
export let name = 'Бизнес сайт';
export let banner = 'src/newpage/business.jpg';
export let pages = [
    {
        name: 'Разработка на шаблоне',
        price: '7 500 руб.',
        duration: '3 дня',
        mainSection: {
            h1: 'Бизнес сайт на шаблоне – это круто',
            h2: 'сайт с проработанной структурой под различные виды деятельности и почти готов к запуску. Требуется минимум времени и затрат для наполнения его вашими информационными материалами.' +
            '<p class="br-blue">В дальнейшем возможен переход на любой тариф</p>',
            p: 'Cпособствует решению множества задач: подача информации о компании, презентация товаров и услуг, создание репутации в интернете, неограниченное количество страниц, прайс – листы, увеличение клиентской базы и доходов, продвижение в Интернете по различным направлениям.'
        },
        specIcons: [
            {
                icon: 'src/newpage/spec-icon-1.png',
                desc: 'Удобная административная панель позволит вам легко вносить изменения на сайте и создавать неограниченное количество страниц. '
            }, {
                icon: 'src/newpage/spec-icon-2.png',
                desc: 'Мы предоставим инструкцию в текстовом и видео формате. Техническая поддержка 7 дней в неделю поможет вам по любым вопросам, которые будут возникать. '
            }
        ],
        scopeSection: [
            {
                p: 'Основные возможности сайта:',
                scopes: [
                    {
                        icon: 'src/newpage/icon-33.png',
                        p: 'мы создаём до 15 страниц, вы можете создать неограниченное количество страниц. '
                    }, {
                        icon: 'src/newpage/icon-11.png',
                        p: 'Удобная система управления сайтом'
                    }, {
                        icon: 'src/newpage/icon-8.png',
                        p: 'Онлайн-консультант'
                    }, {
                        icon: 'src/newpage/icon-9.png',
                        p: 'Интеграция с соц. сетями'
                    }, {
                        icon: 'src/newpage/icon-17.png',
                        p: 'Модуль заказать звонок'
                    }
                ]
            }, {
                p: 'Обслуживание сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-5.png',
                        p: 'Хостинг и домен на 1 год'
                    }, {
                        icon: 'src/newpage/icon-14.png',
                        p: 'Обслуживание сайта на 7 лет'
                    }, {
                        icon: 'src/newpage/icon-16.png',
                        p: 'Техническая поддержка без выходных'
                    }
                ]
            }, {
                p: 'Продвижение сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-1.png',
                        p: 'SEO панель для продвижения'
                    }, {
                        icon: 'src/newpage/icon-3.png',
                        p: 'Управление мета-тегами'
                    }, {
                        icon: 'src/newpage/icon-19.png',
                        p: 'поддержка адресов ЧПУ'
                    }, {
                        icon: 'src/newpage/icon-20.png',
                        p: 'Подключение и установка счетчика Яндекс Метрика'
                    }, {
                        icon: 'src/newpage/icon-13.png',
                        p: 'Регистрация в поисковых системах Yandex.ru, Google.com, Mail.ru'
                    }
                ]
            }
        ],
        shemaSection: [
            {
                h: 'Отправляем до 50 вариантов дизайнов на выбор,',
                p: 'примеры современных дизайнов специально подобраны для вашего бизнеса'
            }, {
                h: 'Наполняем сайт вашими материалами:',
                p: 'тексты, фотографии и картинки, логотип, услуги и товары, ...'
            }, {
                h: 'Размещаем сайт на домене,',
                p: 'запуск сайта, регистрация в Яндекс и Google'
            }
        ],
        praxisSection: [
            {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }, {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }
        ]
    },
    {
        name: 'Под ключ на основе готового решения',
        price: '12 000 руб.',
        duration: '7 дней',
        mainSection: {
            h1: 'Бизнес сайт под ключ на основе готового решения – это',
            h2: 'решение стоит выбрать если вам нравится готовый дизайн но он не подходит полностью и его нужно доработать. Мы подключим дизайнера и программиста, создадим уникальный дизайн на основе шаблона и дополним нужным функционалом. ',
            p: 'Это решение стоит выбрать если вам нравится готовый дизайн но он не подходит полностью и его нужно доработать. Мы подключим дизайнера и программиста, создадим уникальный дизайн на основе шаблона и дополним нужным функционалом. '
        },
        scopeSection: [
            {
                p: 'Основные возможности сайта:',
                scopes: [
                    {
                        icon: 'src/newpage/icon-33.png',
                        p: 'мы создаём 3 страницы, вы можете создать неограниченное количество страниц. '
                    }, {
                        icon: 'src/newpage/icon-11.png',
                        p: 'Удобная система управления сайтом'
                    }, {
                        icon: 'src/newpage/icon-8.png',
                        p: 'Онлайн-консультант'
                    }, {
                        icon: 'src/newpage/icon-9.png',
                        p: 'Интеграция с соц. сетями'
                    }, {
                        icon: 'src/newpage/icon-17.png',
                        p: 'Модуль заказать звонок'
                    }
                ]
            }, {
                p: 'Обслуживание сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-5.png',
                        p: 'Хостинг и домен на 1 год'
                    }, {
                        icon: 'src/newpage/icon-14.png',
                        p: 'Обслуживание сайта на 7 лет'
                    }, {
                        icon: 'src/newpage/icon-16.png',
                        p: 'Техническая поддержка без выходных'
                    }
                ]
            }, {
                p: 'Продвижение сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-1.png',
                        p: 'SEO панель для продвижения'
                    }, {
                        icon: 'src/newpage/icon-3.png',
                        p: 'Управление мета-тегами'
                    }, {
                        icon: 'src/newpage/icon-19.png',
                        p: 'поддержка адресов ЧПУ'
                    }, {
                        icon: 'src/newpage/icon-20.png',
                        p: 'Подключение и установка счетчика Яндекс Метрика'
                    }, {
                        icon: 'src/newpage/icon-13.png',
                        p: 'Регистрация в поисковых системах Yandex.ru, Google.com, Mail.ru'
                    }
                ]
            }
        ],
        shemaSection: [
            {
                h: 'Отправляем до 50 вариантов дизайнов на выбор,',
                p: 'примеры современных дизайнов специально подобраны для вашего бизнеса'
            }, {
                h: 'Наполняем сайт вашими материалами',
                p: 'тексты, фотографии и картинки, логотип, услуги и товары, ...'
            }, {
                h: 'Размещаем сайт на домене,',
                p: 'запуск сайта, регистрация в Яндекс и Google'
            }
        ],
        praxisSection: [
            {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }, {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }
        ]
    },
    {
        name: 'Индивидуальный подход',
        price: 'от 25 000 руб.',
        duration: 'точно в срок',
        mainSection: {
            h1: 'Бизнес сайт с индивидуальным подходом – это',
            h2: 'комплексный подход к созданию сайта через маркетинговый анализ с погружением в сферу бизнеса клиента, изучение его сильных сторон, целевой аудитории и конкурентов.',
            p: '<span class="br-blue">Разрабатывая сайт с нуля под задачи клиента при помощи компетентных специалистов в штате, мы можем гарантировать <b>конверсию сайта не менее 10%.</b><br><b>В стоимость проекта также включен период тестирования сайта.</b>3 месяца после запуска сайта команда аналитиков будет изучать поведение клиентов на каждой странице сайта, для то чтобы скорректировать структуру сайта и сделать её более понятной для пользователей.</span>',
            rightSide: {
                text: 'Мы сделаем сайт именно таким каким вы его представляете',
                small: 'Сдача проекта',
                button: 'Узнать стоимость проекта'
            }
        },
        scopeSection: [
            {
                type: 'bigger',
                p: 'Индивидуальный подход',
                right: 'Разрабатываем сайты <span class="blue">любой сложности, без ограничений по</span> функционалу',
                scopes: [
                    {
                        icon: 'src/newpage/icon-21.png',
                        p: 'Уникальный имиджевый дизайн'
                    }, {
                        icon: 'src/newpage/icon-22.png',
                        p: 'Копирайтинг - уникальные продающие текста'
                    }, {
                        icon: 'src/newpage/icon-23.png',
                        p: 'Маркетинг - анализ рынка, ЦА и конкурентов для лучшего результата'
                    }, {
                        icon: 'src/newpage/icon-24.png',
                        p: 'Скорость загрузки не более 2-х секунд'
                    }
                ]
            }, {
                p: 'Обслуживание сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-5.png',
                        p: 'Хостинг и домен на 1 год'
                    }, {
                        icon: 'src/newpage/icon-14.png',
                        p: 'Обслуживание сайта 7 лет'
                    }, {
                        icon: 'src/newpage/icon-16.png',
                        p: 'Техническая поддержка без выходных'
                    }
                ]
            }, {
                p: 'Продвижение сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-1.png',
                        p: 'SEO панель для продвижения'
                    }, {
                        icon: 'src/newpage/icon-3.png',
                        p: 'Управление мета-тегами'
                    }, {
                        icon: 'src/newpage/icon-19.png',
                        p: 'поддержка адресов ЧПУ'
                    }, {
                        icon: 'src/newpage/icon-20.png',
                        p: 'Подключение и установка счетчика Яндекс Метрика'
                    }, {
                        icon: 'src/newpage/icon-13.png',
                        p: 'Регистрация в поисковых системах Yandex.ru, Google.com, Mail.ru'
                    }
                ]
            }
        ],
        shemaColumn: true,
        shemaRight: {
            h: 'Дополнительно в стоимость входит:',
            desc: '<ul><li>Оптимизация сайта под продвижение</li>' +
            '<li>Составление семантического ядра (ключевых запросов)</li>' +
            '<li>Разработка стратегии продвижения в интернете</li></ul>'
        },
        shemaSection: [
            {
                h: 'Маркетинговый анализ'
            }, {
                h: 'Разработка нескольких прототипов сайта',
                p: 'на основе маркетингового анализа на выбор'
            }, {
                h: 'Разработка уникального дизайна',
                p: 'с возможностью вносить изменения до момента утверждения'
            }, {
                h: 'Адаптивная верстка сайта',
                p: 'под все устройства'
            }, {
                h: 'Программирование',
                p: 'и посадка на систему управления сайтом'
            }, {
                h: 'Подключение модулей обратной связи'
            }, {
                h: 'Регистрация в поисковых системах'
            }, {
                h: 'Тестирование сайта',
                p: 'около 3 месяцев'
            }
        ],
        praxisSection: [
            {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }, {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }
        ]
    }
];