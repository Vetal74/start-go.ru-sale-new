/**
 * Created by Виталий on 16.06.2017.
 */
export let banner = 'src/newpage/contacts.jpg';
export let template = `
        <header>
            <div class="wrapper-gradient">
                <div class="breadcrumbs">
                    <a href="/">Главная</a>
                    <span class="delimer"> - </span>
                    <p class="current">Контакты</p>
                </div>
                <h1 class="center">Контакты</h1>
            </div>
        </header>
        <div class="wrapper-section non-padding">
            <section class="contacts">
                <div class="call">
                    <a href="tel:+78005005062" class="tel">8 800 500-50-62</a>
                    <p class="small">(Бесплатно для всей России)</p>
                    <button class="button extra-blackblue b-call">
                        <span>Заказать звонок</span>
                    </button>
                </div>
                <div class="email">
                    <a href="mailto:info@start-go.ru" class="mail">
                        <span class="icon"><img class="lazy-load" data-src="src/newpage/email.png" src="" alt=""></span>
                        <span class="text">info@start-go.ru</span>
                    </a>
                    <p class="small">Онлайн консультация:</p>
                    <button class="button extra-blackblue b-chat"><span>Онлайн-консультант</span></button>
                </div>
            </section>
        </div>
        <div class="contact-sec">
            <form class="form form-contacts" action="" novalidate>
                <h2>Написать нам:</h2>
                <div class="line">
                    <div>
                        <input type="text" name="name" required>
                        <label><span>Имя</span></label>
                    </div>
                </div>
                <div class="line">
                    <div>
                        <input type="tel" name="tel" required>
                        <label><span>Телефон</span></label>
                    </div>
                    <div>
                        <input type="text" name="email" required>
                        <label><span>Ваш e-mail</span></label>
                    </div>
                </div>
                <div class="line">
                    <div class="textarea">
                        <textarea name="message" required></textarea>
                        <label><span>Ваше сообщение</span></label>
                    </div>
                </div>
                <button class="button extra-blackblue"><span>Отправить</span></button>
            </form>
        </div>
        <footer class="main-footer">
            <div class="wrapper">
                <div class="footer-block contacts">
                    <h4>Контакты</h4>
                    <div class="links">
                        <div class="link b-chat">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m1.png" src="" alt="">
                            </div>
                            <a class="consult" href="">Онлайн косультант</a>
                        </div>
                        <div class="link b-chat">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m2.png" src="" alt="">
                            </div>
                            <a class="tehpod open-modal" data-modal="modal-write-us" href="">Техническая поддержка</a>
                        </div>
                        <div class="link b-chat">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m3.png" src="" alt="">
                            </div>
                            <a class="obr open-modal" data-modal="modal-write-us" href="">Обратная связь</a>
                        </div>
                        <div class="link">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m4.png" src="" alt="">
                            </div>
                            <a class="mail" href="mailto:info@start-go.ru">info@start-go.ru</a>
                        </div>
                    </div>
                    <div class="callus">
                        <a href="tel:+78005005062" class="tel">8 800 500-50-62</a>
                        <p class="podtel">(Бесплатный звонок для всей России)</p>
                        <button class="button footer-button b-call"><span>Заказать звонок</span></button>
                    </div>
                </div>
                <div class="footer-block uslugi">
                    <h4>Услуги</h4>
                    <div class="links">
                        <ul>
                            <li>
                                <span>-</span><a href="visitka">Сайт визитка</a>
                            </li>
                            <li>
                                <span>-</span><a href="company">Бизнес сайт</a>
                            </li>
                            <li>
                                <span>-</span><a href="onlineShop">Интернет-магазин</a>
                            </li>
                            <li>
                                <span>-</span><a href="landing">Landing page</a>
                            </li>
                            <li>
                                <span>-</span><a href="seo">Продвижение</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="footer-block info">
                    <h4>Информация</h4>
                    <div class="links">
                        <ul>
                            <li>
                                <span>-</span><a href="visitka">Сайт визитка</a>
                            </li>
                            <li>
                                <span>-</span><a href="company">Бизнес сайт</a>
                            </li>
                            <li>
                                <span>-</span><a href="onlineShop">Интернет-магазин</a>
                            </li>
                            <li>
                                <span>-</span><a href="landing">Landing page</a>
                            </li>
                            <li>
                                <span>-</span><a href="seo">Продвижение</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="footer-block social">
                    <h4>Мы в соц. сетях</h4>
                    <div class="links">
                        <a href="https://vk.com/startgo" target="_blank">
                            <div class="link">
                                <div class="icon">
                                    <img src="" data-src="src/vk.png" alt="" class="lazy-load">
                                </div>
                                Вконтакте
                            </div>
                        </a>
                        <a href="https://www.instagram.com/startgo.pro/" target="_blank">
                            <div class="link">
                                <div class="icon">
                                    <img src="" data-src="src/insta.png" alt="" class="lazy-load">
                                </div>
                                Инстаграм
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
`;