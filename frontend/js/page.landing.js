/**
 * Created by Виталий on 07.06.2017.
 */
export let name = 'Landing page';
export let banner = 'src/newpage/landing.jpg';
export let pages = [
    {
        name: 'Разработка на шаблоне',
        price: '25 000 руб.',
        duration: '7 дней',
        mainSection: {
            h1: 'Landing page на шаблоне - это',
            h2: 'фвыато одностраничный симпатичный сайт с акцентом на продажу конкретных услуг, товаров, акций.'
        },
        scopeSection: [
            {
                p: 'Основные возможности сайта:',
                scopes: [
                    {
                        icon: 'src/newpage/icon-27.png',
                        p: 'Уникальный имиджевый дизайн'
                    }, {
                        icon: 'src/newpage/icon-25.png',
                        p: 'Маркетинг - анализ рынка, ЦА и конкурентов для лучшего результата'
                    }, {
                        icon: 'src/newpage/icon-29.png',
                        p: 'Сквозная аналитика'
                    }, {
                        icon: 'src/newpage/icon-31.png',
                        p: 'Копирайтинг - уникальные продающие текста'
                    }, {
                        icon: 'src/newpage/icon-26.png',
                        p: 'Скорость загрузки не более 2-х секунд'
                    }, {
                        icon: 'src/newpage/icon-32.png',
                        p: 'А/Б тестирование'
                    }, {
                        icon: 'src/newpage/icon-30.png',
                        p: 'Гарантия конверсии не менее 10%'
                    }, {
                        icon: 'src/newpage/icon-11.png',
                        p: 'Удобная административная панель'
                    }
                ]
            }, {
                p: 'Обслуживание сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-5.png',
                        p: 'Хостинг и домен на 1 год'
                    }, {
                        icon: 'src/newpage/icon-14.png',
                        p: 'Обслуживание сайта на 7 лет'
                    }, {
                        icon: 'src/newpage/icon-16.png',
                        p: 'Техническая поддержка без выходных'
                    }
                ]
            }, {
                p: 'Продвижение сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-1.png',
                        p: 'SEO панель для продвижения'
                    }, {
                        icon: 'src/newpage/icon-3.png',
                        p: 'Управление мета-тегами'
                    }, {
                        icon: 'src/newpage/icon-28.png',
                        p: 'Помощь в настройке контекстной рекламы (Яндекс.Директ, Гугл эдвордс)'
                    }, {
                        icon: 'src/newpage/icon-20.png',
                        p: 'Подключение и установка счетчика Яндекс Метрика'
                    }, {
                        icon: 'src/newpage/icon-13.png',
                        p: 'Регистрация в поисковых системах Yandex.ru, Google.com, Mail.ru'
                    }
                ]
            }
        ],
        shemaSection: [
            {
                h: 'Выбираем вариант шаблона и дизайна'
            }, {
                h: 'Наполняем вашими материалами сайт'
            }, {
                h: 'Размещаем на домене и на хостинге'
            }, {
                h: 'Регистрируем в поисковых системах Яндекс и Гугл'
            }
        ],
        praxisSection: [
            {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }, {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }
        ]
    },
    {
        name: 'Под ключ с уникальным дизайном',
        price: '25 000 руб.',
        duration: '7 дней',
        mainSection: {
            h1: 'Landing page под ключ с уникальным дизайном - это',
            h2: 'это одностраничный симпатичный сайт с акцентом на продажу конкретных услуг, товаров, акций.'
        },
        scopeSection: [
            {
                p: 'Основные возможности сайта:',
                scopes: [
                    {
                        icon: 'src/newpage/icon-27.png',
                        p: 'Уникальный имиджевый дизайн'
                    }, {
                        icon: 'src/newpage/icon-25.png',
                        p: 'Маркетинг - анализ рынка, ЦА и конкурентов для лучшего результата'
                    }, {
                        icon: 'src/newpage/icon-29.png',
                        p: 'Сквозная аналитика'
                    }, {
                        icon: 'src/newpage/icon-31.png',
                        p: 'Копирайтинг - уникальные продающие текста'
                    }, {
                        icon: 'src/newpage/icon-26.png',
                        p: 'Скорость загрузки не более 2-х секунд'
                    }, {
                        icon: 'src/newpage/icon-32.png',
                        p: 'А/Б тестирование'
                    }, {
                        icon: 'src/newpage/icon-30.png',
                        p: 'Гарантия конверсии не менее 10%'
                    }, {
                        icon: 'src/newpage/icon-11.png',
                        p: 'Удобная административная панель'
                    }
                ]
            }, {
                p: 'Обслуживание сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-5.png',
                        p: 'Хостинг и домен на 1 год'
                    }, {
                        icon: 'src/newpage/icon-14.png',
                        p: 'Обслуживание сайта на 7 лет'
                    }, {
                        icon: 'src/newpage/icon-16.png',
                        p: 'Техническая поддержка без выходных'
                    }
                ]
            }, {
                p: 'Продвижение сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-1.png',
                        p: 'SEO панель для продвижения'
                    }, {
                        icon: 'src/newpage/icon-3.png',
                        p: 'Управление мета-тегами'
                    }, {
                        icon: 'src/newpage/icon-28.png',
                        p: 'Помощь в настройке контекстной рекламы (Яндекс.Директ, Гугл эдвордс)'
                    }, {
                        icon: 'src/newpage/icon-20.png',
                        p: 'Подключение и установка счетчика Яндекс Метрика'
                    }, {
                        icon: 'src/newpage/icon-13.png',
                        p: 'Регистрация в поисковых системах Yandex.ru, Google.com, Mail.ru'
                    }
                ]
            }
        ],
        shemaSection: [
            {
                h: 'Создание 3-х прототипов на выбор'
            }, {
                h: 'Разработка дизайна'
            }, {
                h: 'Верстка и программирование'
            }, {
                h: 'Тестирование сайта и аудит'
            }
        ],
        praxisSection: [
            {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }, {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }
        ]
    },
    {
        name: 'Под ключ + маркетинг',
        price: 'от 45 000 руб.',
        duration: 'точно в срок',
        specPredl: {
            p: 'Бесплатный прототип вашего лендинга в течение 24 часов!',
            buttonText: 'Получить',
            class: 'landing-spec'
        },
        mainSection: {
            h1: 'Landing page под ключ + маркетинг - это',
            h2: 'одностраничный симпатичный сайт с акцентом на продажу конкретных услуг, товаров, акций.',
            p: 'Благодаря высокому уровню и широким знаниям наших специалистов мы можем гарантировать <b>коверсию сайта не менее 10%.</b>',
            rightSide: {
                text: 'Мы сделаем сайт именно таким каким вы его представляете',
                small: 'Сдача проекта',
                button: 'Узнать стоимость проекта'
            }
        },
        scopeSection: [
            {
                p: 'Основные возможности сайта:',
                scopes: [
                    {
                        icon: 'src/newpage/icon-27.png',
                        p: 'Уникальный имиджевый дизайн'
                    }, {
                        icon: 'src/newpage/icon-25.png',
                        p: 'Маркетинг - анализ рынка, ЦА и конкурентов для лучшего результата'
                    }, {
                        icon: 'src/newpage/icon-29.png',
                        p: 'Сквозная аналитика'
                    }, {
                        icon: 'src/newpage/icon-31.png',
                        p: 'Копирайтинг - уникальные продающие текста'
                    }, {
                        icon: 'src/newpage/icon-26.png',
                        p: 'Скорость загрузки не более 2-х секунд'
                    }, {
                        icon: 'src/newpage/icon-32.png',
                        p: 'А/Б тестирование'
                    }, {
                        icon: 'src/newpage/icon-30.png',
                        p: 'Гарантия конверсии не менее 10%'
                    }, {
                        icon: 'src/newpage/icon-11.png',
                        p: 'Удобная административная панель'
                    }
                ]
            }, {
                p: 'Обслуживание сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-5.png',
                        p: 'Хостинг и домен на 1 год'
                    }, {
                        icon: 'src/newpage/icon-14.png',
                        p: 'Обслуживание сайта на 7 лет'
                    }, {
                        icon: 'src/newpage/icon-16.png',
                        p: 'Техническая поддержка без выходных'
                    }
                ]
            }, {
                p: 'Продвижение сайта',
                scopes: [
                    {
                        icon: 'src/newpage/icon-1.png',
                        p: 'SEO панель для продвижения'
                    }, {
                        icon: 'src/newpage/icon-3.png',
                        p: 'Управление мета-тегами'
                    }, {
                        icon: 'src/newpage/icon-28.png',
                        p: 'Помощь в настройке контекстной рекламы (Яндекс.Директ, Гугл эдвордс)'
                    }, {
                        icon: 'src/newpage/icon-20.png',
                        p: 'Подключение и установка счетчика Яндекс Метрика'
                    }, {
                        icon: 'src/newpage/icon-13.png',
                        p: 'Регистрация в поисковых системах Yandex.ru, Google.com, Mail.ru'
                    }
                ]
            }
        ],
        shemaColumn: true,
        shemaSection: [
            {
                h: 'Маркетинговый анализ'
            }, {
                h: 'Создание 3-х прототипов на выбор',
                p: 'на основе маркетингового анализа'
            }, {
                h: 'Разработка уникального дизайна',
                p: 'С возможностью вносить изменения до момента утверждения'
            }, {
                h: 'Верстка и программирование'
            }, {
                h: 'Тестирование 2 месяца',
                p: 'и внесение правок'
            }
        ],
        praxisSection: [
            {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }, {
                bigImage: 'src/newpage/small_image.png',
                smallImage: 'src/newpage/small_image.png'
            }
        ]
    }
];