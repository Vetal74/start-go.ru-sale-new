/**
 * Created by Виталий on 16.06.2017.
 */
export let banner = 'src/newpage/prodv.jpg';
export let template = `
        <header>
            <div class="wrapper-gradient">
                <div class="breadcrumbs">
                    <a href="/">Главная</a>
                    <span class="delimer"> - </span>
                    <p class="current">Продвижение</p>
                </div>
                <h1 class="center">Продвижение</h1>
            </div>
        </header>
        <div class="wrapper-section non-padding">
            <section class="main-section visi-anim seo">
                <div class="left-side">
                    <h2>Наша комания использует комплексный подход к продвижению интернет проектов</h2>
                    <p class="head">это помогает достичь высоких результатов и максимульно использовать все возможности интернета</p>
                    <div class="line-blue">
                        Мы гарантированно приведем целевую аудиторию на ваш сайт и превратим их в клиентов.
                    </div>
                </div>
                <img data-src="src/newpage/arrow-seo.png" src="" alt="" class="lazy-load seo-arrow">
                <img data-src="src/newpage/nout-seo.png" src="" alt="" class="lazy-load seo-nout">
                <img data-src="src/newpage/shadow-seo.png" src="" class="lazy-load seo-shadow" alt="">
            </section>
            <section class="scope-section seo visi-anim">
                <h2>Способы продвижения, в которых <br>мы являемся специалистами</h2>
                <div class="blocks">
                    <div class="block">
                        <div class="icon">
                            <img class="lazy-load" data-src="src/newpage/icon-seo-4.png" src="" alt="">
                        </div>
                        <div class="description">
                            <p class="h">SEO</p>
                            <p class="d">(в поисковых системах)</p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="icon">
                            <img class="lazy-load" data-src="src/newpage/icon-seo-5.png" src="" alt="">
                        </div>
                        <div class="description">
                            <p class="h">Промо видео</p>
                            <p class="d">(youtube, телевидение...)</p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="icon">
                            <img class="lazy-load" data-src="src/newpage/icon-seo-2.png" src="" alt="">
                        </div>
                        <div class="description">
                            <p class="h">Таргетированная реклама</p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="icon">
                            <img class="lazy-load" data-src="src/newpage/icon-seo-6.png" src="" alt="">
                        </div>
                        <div class="description">
                            <p class="h">SMM/SMO</p>
                            <p class="d">(в соц. сетях)</p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="icon">
                            <img class="lazy-load" data-src="src/newpage/icon-seo-1.png" src="" alt="">
                        </div>
                        <div class="description">
                            <p class="h">Контекстная реклама</p>
                            <p class="d">(Яндекс.Директ, Google.Adwords)</p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="icon">
                            <img class="lazy-load" data-src="src/newpage/icon-seo-3.png" src="" alt="">
                        </div>
                        <div class="description">
                            <p class="h">Аналитика и A/B тестирование</p>
                            <p class="d">для максимального эффекта от рекламы</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="blueSec">
            <div class="wrapper-section blue">
                <section class="consult visi-anim">
                    <div class="left-side">
                        <p class="head">Консультируем по всем вопросам: </p>
                        <a href="tel:+78005005062" class="tel">8 800 500-50-62</a>
                        <p class="podtel">(бесплатная горячая линия для РФ)</p>
                        <a href="" class="sale-call b-call">Заказать звонок</a>
                        <button class="button yellow open-modal" data-modal="modal-write-us">
                            <span>Заказать сайт</span>
                        </button>
                    </div>
                    <div class="right-side lazy-load">

                    </div>
                </section>
            </div>
        </div>
        <footer class="main-footer">
            <div class="wrapper">
                <div class="footer-block contacts">
                    <h4>Контакты</h4>
                    <div class="links">
                        <div class="link b-chat">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m1.png" src="" alt="">
                            </div>
                            <a class="consult" href="">Онлайн косультант</a>
                        </div>
                        <div class="link b-chat">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m2.png" src="" alt="">
                            </div>
                            <a class="tehpod open-modal" data-modal="modal-write-us" href="">Техническая поддержка</a>
                        </div>
                        <div class="link b-chat">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m3.png" src="" alt="">
                            </div>
                            <a class="obr open-modal" data-modal="modal-write-us" href="">Обратная связь</a>
                        </div>
                        <div class="link">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m4.png" src="" alt="">
                            </div>
                            <a class="mail" href="mailto:info@start-go.ru">info@start-go.ru</a>
                        </div>
                    </div>
                    <div class="callus">
                        <a href="tel:+78005005062" class="tel">8 800 500-50-62</a>
                        <p class="podtel">(Бесплатный звонок для всей России)</p>
                        <button class="button footer-button b-call"><span>Заказать звонок</span></button>
                    </div>
                </div>
                <div class="footer-block uslugi">
                    <h4>Услуги</h4>
                    <div class="links">
                        <ul>
                            <li>
                                <span>-</span><a href="visitka">Сайт визитка</a>
                            </li>
                            <li>
                                <span>-</span><a href="company">Бизнес сайт</a>
                            </li>
                            <li>
                                <span>-</span><a href="onlineShop">Интернет-магазин</a>
                            </li>
                            <li>
                                <span>-</span><a href="landing">Landing page</a>
                            </li>
                            <li>
                                <span>-</span><a href="seo">Продвижение</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="footer-block info">
                    <h4>Информация</h4>
                    <div class="links">
                        <ul>
                            <li>
                                <span>-</span><a class="navigation" href="visitka">О нас</a>
                            </li>
                            <li>
                                <span>-</span><a class="navigation" href="company">Портфолио</a>
                            </li>
                            <li>
                                <span>-</span><a class="navigation" href="onlineShop">Цены на услуги</a>
                            </li>
                            <li>
                                <span>-</span><a class="navigation" href="landing">Партнерская программа</a>
                            </li>
                            <li>
                                <span>-</span><a class="navigation" href="seo">Реквизиты</a>
                            </li>
                            <li>
                                <span>-</span><a class="navigation" href="seo">Карта сайта</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="footer-block social">
                    <h4>Мы в соц. сетях</h4>
                    <div class="links">
                        <a href="https://vk.com/startgo" target="_blank">
                            <div class="link">
                                <div class="icon">
                                    <img src="" data-src="src/vk.png" alt="" class="lazy-load">
                                </div>
                                Вконтакте
                            </div>
                        </a>
                        <a href="https://www.instagram.com/startgo.pro/" target="_blank">
                            <div class="link">
                                <div class="icon">
                                    <img src="" data-src="src/insta.png" alt="" class="lazy-load">
                                </div>
                                Инстаграм
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
`;