/**
 * Created by Виталий on 28.06.2017.
 */
import ResLoader from './loader';
import MaskInput from './maskInput';

export default class PageWalker {
    constructor(props = {}) {
        this.props = {
            newPageClass: props.newPageClass || 'new-page',
            closePageClass: props.closePageClass || 'close-new-page',
            isOur: props.isOur || false,
            href: props.href || false,
            attrData: props.attrData || {},
            timeToOpen: props.timeToOpen || 750,
            mask: props.mask || false,
            ajax: props.ajax || false,
            portfolio: props.portfolio || false,
            wrapper: props.wrapper || 'wrapper-section'
        };
        this.thisPageData = {};
        this.mainPortfolioData = {};
        this.leftNavPortfolio = 0;
        this.newPage = {};
        this.going = 0;
        this.maxGoing = 0;
        this.ajaxData = 0;
        this.isOpen = false;
    }
    open() {
        let self = this;
        this.isOpen = true;
        PageWalker.offBodyScroll();
        $('.layout').addClass('active');
        if(this.props.ajax) {
            new Promise((resolve, reject) => {
                resolve(() => self.getAjaxData(self));
            })
                .then(result => {
                    return result(self);
                })
                .then(result => {
                    self.ajaxData = result;
                    self.getPage(result);
                    return true;
                })
                .catch(err => console.error(err));
        } else {
            import(`./page.${this.props.href}.js`)
                .then(pageData => {
                    this.thisPageData = pageData;
                    this.getPage(pageData);
                })
                .catch(e => new Error(e));
        }
    }
    change() {
        let self = this;
        if(!this.isOpen) {
            this.open();
            return false;
        }
        if(this.props.ajax) {
            new Promise(resolve => {
                resolve(() => self.getAjaxData(self, true));
            })
                .then(result => {
                    self.ajaxData = result;
                    self.changePage(result);
                })
                .catch(err => console.log(err));
        }
    }
    changePage(data) {

    }
    getPage(data) {
        this.newPage = $(`.${this.props.newPageClass}`);
        $(`.${this.props.newPageClass}, .${this.props.closePageClass}`).addClass('active');
        this.loadTitle(data.name);
        if(!this.props.isOur) {
            this.loadTabs();
            this.pasteDataDef(data.pages[0]);
        }
        else if(this.props.portfolio) {
            this.pastePortfolio(data);
        }
        else this.stupidPaste(data.template);
        this.loadResourses(data.banner);
    }
    getAjaxData(self, change = false) {
        if(self.mainPortfolioData.length > 0 && !self.props.portfolio) {
            return new Promise(resolve => {
                resolve(self.pasteAjaxData(self.mainPortfolioData, self, change, true));
            });
        } else {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: self.props.href,
                    type: 'POST',
                    success: data => {
                        self.thisPageData = data;
                        if(self.props.portfolio)
                            resolve(self.pasteAjaxDataPortfolio(data, self, change));
                        else
                            resolve(self.pasteAjaxData(data, self, change));
                    },
                    error: err => {
                        console.error(err);
                    }
                });
            });
        }
    }
    pasteAjaxData(data, self, change, cache = false) {
        if(cache) return data;
        const dataToPaste = {
            banner: 'src/company-back.jpg'
        };
        let htmlData = 0;
        htmlData = $(data).filter('.ajaxPaste').find('.portWrap');
        if(htmlData !== 0 && !(PageWalker.isUndefined(htmlData))){
            htmlData = htmlData.html();
        } else {
            console.error('Почему-то в ответе от серва нет того, что надо! >:');
        }
        if(change) {

        }
        dataToPaste.template = self.templatePortfolioAll(htmlData);
        dataToPaste.name = 'Портфолио';
        self.mainPortfolioData = dataToPaste;
        return dataToPaste;
    }
    pasteAjaxDataPortfolio(data, self, change) {
        const dataToPaste = {
            banner: 'src/company-back.jpg'
        };
        let htmlData = 0;
        let h1 = 0;
        if(self.leftNavPortfolio === 0) self.leftNavPortfolioInit();
        htmlData = $(data).filter('.ajaxPaste').find('.wrap-port img').attr('src');
        h1 = $(data).filter('.ajaxPaste').find('h1').html();
        if(change) {
            dataToPaste.name = h1;
            dataToPaste.img = htmlData;
            return dataToPaste;
        }
        dataToPaste.template = self.templatePortfolioPageAll(htmlData, h1);
        dataToPaste.name = h1;
        return dataToPaste;
    }
    stupidPaste(template) {
        this.newPage.html(template);
    }
    loadTitle(name) {
        this.newPage.find('header .current, header h1')
            .html(name);
    }
    loadTabs() {
        const data = this.thisPageData;
        let self = this;
        this.newPage.find('section.tabs > p').html(`${data.pages.length} варианта разработки:`);
        let tabsContainer = this.newPage.find('.tabs-container'),
            htmlTabs = '';
        data.pages.forEach((page, key) => {
            let active = key === 0? ' active' : '';
            htmlTabs += `
            <div class="tab${active}" data-tab="${key}">
                <div class="wrap-tab">
                    <p class="name">${page.name}</p>
                    <div class="bottom">
                        <p class="price">${page.price}</p>
                        <a href="">Подробнее</a>
                        <div class="arrow-bottom lazy-load"></div>
                    </div>
                </div>
            </div>
        `;
        });
        tabsContainer.html(htmlTabs);
        $('.new-page .tabs-container').on('click', '.tab, a', function() {
            let $this = $(this);
            if(!$this.hasClass('tab')) $this = $this.parents('.tab');
            if($this.hasClass('active')) return false;
            $('.new-page .tabs-container .tab').removeClass('active');
            $this.addClass('active');
            $('.new-page > .wrapper-section > *').toggleClass('visi-anim hide-anim');
            setTimeout(() => {
                self.pasteDataDef(self.thisPageData.pages[$this.data('tab')]);
                $('.new-page > .wrapper-section > *').toggleClass('visi-anim hide-anim');
                ResLoader.loadImages('.new-page img.lazy-load', '.new-page div.lazy-load');
            }, 350);
            return false;
        });
    }
    loadResourses(banner) {
        const newPage = this.newPage;
        setTimeout(() => {
            let mainImage = document.createElement('img');
            mainImage.src = banner;
            mainImage.onload = () => {
                newPage.find('>header').css('background-image', `url(${banner})`);
                newPage.find('>header .wrapper-gradient').addClass('hide');
            };
            ResLoader.loadImages(`.${this.props.newPageClass} img.lazy-load`, `.${this.props.newPageClass} div.lazy-load`);
            if(this.props.mask) MaskInput.loadMask(MaskInput.initMask);
        }, this.props.timeToOpen);
    }
    pasteDataDef(page) {
        let self = this;
        try {
            let mainSection = this.newPage.find('.main-section');
            mainSection.find('h2').html(page.mainSection.h1);
            mainSection.find('.left-side p.head').html(page.mainSection.h2);
            if(!PageWalker.isUndefined(page.mainSection.p))
                mainSection.find('.left-side p.desc').html(page.mainSection.p);
            else mainSection.find('.left-side p.desc').empty();
            mainSection.find('p.price').html(page.price);
            mainSection.find('.duration p.big').html(page.duration);
            if(!PageWalker.isUndefined(page.mainSection.rightSide)) {
                let rightSide = mainSection.find('.right-side');
                rightSide.addClass('ind');
                rightSide.find('.desc').html(page.mainSection.rightSide.text);
                rightSide.find('p.small').html(page.mainSection.rightSide.small);
                rightSide.find('button span').html(page.mainSection.rightSide.button);
            } else {
                let rightSide = mainSection.find('.right-side');
                rightSide.removeClass('ind');
                rightSide.find('.desc').html('Окончательная стоимость полностью готового сайта');
                rightSide.find('p.small').html('Срок разработки:');
                rightSide.find('button span').html('Заказать сайт');
            }

            if(self.newPage.find('.specSec').length != 0)
                self.newPage.find('.specSec').detach();
            if(!PageWalker.isUndefined(page.specPredl)) {
                let htmlSpec = `
                    <div class="specSec lazy-load visi-anim ${page.specPredl.class}">
                        <div class="wrapper-section">
                            <div class="left">
                                <p>${page.specPredl.p}</p>
                                <button class="button yellow open-modal" data-modal="modal-write-us"><span>${page.specPredl.buttonText}</span></button>
                            </div>
                        </div>
                    </div>
                `;
                pasteSpec(htmlSpec);
            } else if(!PageWalker.isUndefined(page.specIcons)) {
                let htmlSpec = `
                    <div class="specSec lazy-load visi-anim specIcons">
                        <div class="wrapper-section">
                            `;
                        page.specIcons.forEach(spec => {
                            htmlSpec += `
                        <div class="block">
                            <div class="icon">
                                <img class="lazy-load" data-src="${spec.icon}" src="" alt="">
                            </div>
                            <div class="desc">
                                ${spec.desc}
                            </div>
                        </div> 
                    `;
                });
                htmlSpec += `
                        </div>
                    </div>
                `;
                pasteSpec(htmlSpec);
            }

            function pasteSpec(htmlSpec) {
                let firstWrapper = self.newPage.find('>.wrapper-section:nth-of-type(1)');
                if(!firstWrapper.hasClass('separate')) {
                    firstWrapper.after('<div class="wrapper-section"></div>');
                    firstWrapper.find('.scope-section, .shema-section').clone()
                        .appendTo(self.newPage.find('>.wrapper-section:nth-of-type(2)'));
                    firstWrapper.find('.scope-section, .shema-section').detach();
                    firstWrapper.addClass('separate');
                }
                firstWrapper.after(htmlSpec);
            }

            let scopeSectionBlocks = self.newPage.find('.scope-section .blocks'),
                htmlBlocks = '';
            page.scopeSection.forEach(section => {
                if(!PageWalker.isUndefined(section.type)) {
                    htmlBlocks += `
                        <div class="block bigger">
                            <p class="head">${section.p}</p>
                            <div class="scopes">
                    `;
                    section.scopes.forEach(scope => {
                        htmlBlocks += `
                            <div class="scope">
                                <div class="icon">
                                    <img class="lazy-load" src="" data-src="${scope.icon}" alt="">
                                </div>
                                <div class="desc">
                                    <p>${scope.p}</p>
                                </div>
                            </div>
                        `;
                    });
                    htmlBlocks += `</div>
                        <div class="right-scope">${section.right}</div>
                    </div>`;
                } else {
                    htmlBlocks += `
                        <div class="block">
                            <p class="head">${section.p}</p>
                            <div class="scopes">
                    `;
                    section.scopes.forEach(scope => {
                        htmlBlocks += `
                            <div class="scope">
                                <div class="icon">
                                    <img class="lazy-load" src="" data-src="${scope.icon}" alt="">
                                </div>
                                <div class="desc">
                                    <p>${scope.p}</p>
                                </div>
                            </div>
                        `;
                    });
                    htmlBlocks += `</div></div>`;
                }
            });
            scopeSectionBlocks.html(htmlBlocks);
            let shemaSectionBlocks = self.newPage.find('.shema-section .blocks'),
                htmlShema = '';
            if(!PageWalker.isUndefined(page.shemaColumn)) shemaSectionBlocks.addClass('column');
            else shemaSectionBlocks.removeClass('column');
            if(page.shemaSection.length === 4) shemaSectionBlocks.addClass('four');
            else shemaSectionBlocks.removeClass('four');
            page.shemaSection.forEach(section => {
                if(PageWalker.isUndefined(section.p)) section.p = '';
                htmlShema += `
                    <div class="block">
                        <p class="head">${section.h}</p>
                        <p class="desc">${section.p}</p>
                    </div>
                `;
            });
            if(!PageWalker.isUndefined(page.shemaRight)) {
                htmlShema += `<div class="right-block">
                    <p class="h">${page.shemaRight.h}</p>
                    ${page.shemaRight.desc}
                </div>`;
            }
            shemaSectionBlocks.html(htmlShema);
            let praxisSectionImages = self.newPage.find('.praxis-section .images'),
                htmlImages = '';
            page.praxisSection.forEach(section => {
                htmlImages += `<div class="block-image">`;
                if(PageWalker.isUndefined(page.praxisImagesNepara))
                    htmlImages += `<div class="before-after">
                                        <p class="before">До</p>
                                        <img class="lazy-load" data-src="src/newpage/gradi-arrow.png" src="" alt="">
                                        <p class="after">После</p>
                                    </div>`;
                htmlImages += `
                        <div class="image ${PageWalker.isUndefined(page.praxisImagesNepara) ? '' : 'shadow'}">
                            <a class="scaleBox" href="${section.bigImage}"><img class="lazy-load" src="" data-src="${section.smallImage}" alt=""></a>
                        </div>
                    </div>
                `;
            });
            praxisSectionImages.html(htmlImages);
            if(self.newPage.find('.all-desc-section').length > 0)
                self.newPage.find('.all-desc-section').parent().remove();
            if(!PageWalker.isUndefined(page.allDescSection)) {
                let htmlDesc = `
                <div class="wrapper-section">
                    <section class="all-desc-section visi-anim">
                        <h2>Полное описание <br>возможностей сайта</h2>
                        <div class="blocks">
                `;
                page.allDescSection.forEach(section => {
                    htmlDesc += `
                        <div class="block">
                            <p class="head">${section.p}</p>
                            <ul>
                    `;
                    let counter = 0;
                    section.ul.forEach(li => {
                        htmlDesc += `
                            <li><span>${++counter}.</span> ${li}</li>
                        `;
                    });
                    htmlDesc += `</ul></div>`;
                });
                htmlDesc += `
                    </div>
                        </section>
                        <footer class="visi-anim">
                            <p>${page.footer}</p>
                        </footer>
                    </div>
                `;
                self.newPage.find('footer.main-footer').before(htmlDesc);
            }
        } catch (e) {
            console.error(e);
        }
    }
    pastePortfolio(data) {
        $(`.${this.props.newPageClass}`).html(data.template);
        // const wrapperPortfolio = $(`.${this.props.wrapper}`);
        // wrapperPortfolio.html(data.template);
        // wrapperPortfolio.removeClass('hide-anim')
        //     .addClass('visi-anim project-wrapper');
    }
    close() {
        let self = this;
        this.isOpen = false;
        $(`.layout, .${this.props.newPageClass}, .${this.props.closePageClass}`).removeClass('active');
        PageWalker.offBodyScroll(false);
        $(`.${this.props.newPageClass}>header .wrapper-gradient`).removeClass('hide');
        //this.leftNavPortfolio = 0;
        $('.infinite-block .wrap').off('scroll');
        $('.new-page .tabs-container').off('click', '.tab a');
        return new Promise(resolve => {
            setTimeout(() => {
                $(`.${self.props.newPageClass}`).scrollTop(0);
                resolve();
            }, 750);
        });
    }
    templatePortfolio(htmlData) {
        return `
            ${htmlData}
        `;
    }
    templatePortfolioAll(htmlData) {
        return `
            <header>
                <div class="wrapper-gradient">
                    <div class="breadcrumbs">
                        <a href="/">Главная</a>
                        <span class="delimer"> - </span>
                        <p class="current">Портфолио</p>
                    </div>
                    <h1 class="center">Портфолио</h1>
                </div>
            </header>
            <div class="wrapper-section non-padding portfolio-wrapper">
                ${htmlData}
            </div>
            ${this.templateFooter()}
    `;
    }
    templatePortfolioPage(htmlData, h1) {
        return `
                <img class="lazy-load project-img" src="" data-src="${htmlData}" alt="">
                <button class="button extra-blackblue open-modal" data-modal="modal-write-us"><span>Хочу подобный сайт</span></button>
            `;
    }
    templatePortfolioPageAll(htmlData, h1) {
        return `
            <header>
                <div class="wrapper-gradient">
                    <div class="breadcrumbs">
                        <a href="/">Главная</a>
                        <span class="delimer"> - </span>
                        <a href="" class="portfolio">Портфолио</a>
                        <span class="delimer"> - </span>
                        <p class="current">${h1}</p>
                    </div>
                    <h1 class="center">${h1}</h1>
                </div>
            </header>
            <div class="wrapper-section non-padding project-wrapper">
                <img class="lazy-load project-img" src="" data-src="${htmlData}" alt="">
                <button class="button extra-blackblue open-modal" data-modal="modal-write-us"><span>Хочу подобный сайт</span></button>
            </div>
            ${this.templateFooter()}
        `;
    }
    templateFooter() {
        return `
        <footer class="main-footer">
            <div class="wrapper">
                <div class="footer-block contacts">
                    <h4>Контакты</h4>
                    <div class="links">
                        <div class="link b-chat">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m1.png" src="" alt="">
                            </div>
                            <a class="consult" href="">Онлайн косультант</a>
                        </div>
                        <div class="link b-chat">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m2.png" src="" alt="">
                            </div>
                            <a class="tehpod open-modal" data-modal="modal-write-us" href="">Техническая поддержка</a>
                        </div>
                        <div class="link b-chat">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m3.png" src="" alt="">
                            </div>
                            <a class="obr open-modal" data-modal="modal-write-us" href="">Обратная связь</a>
                        </div>
                        <div class="link">
                            <div class="icon">
                                <img class="lazy-load" data-src="src/m4.png" src="" alt="">
                            </div>
                            <a class="mail" href="mailto:info@start-go.ru">info@start-go.ru</a>
                        </div>
                    </div>
                    <div class="callus">
                        <a href="tel:+78005005062" class="tel">8 800 500-50-62</a>
                        <p class="podtel">(Бесплатный звонок для всей России)</p>
                        <button class="button footer-button b-call"><span>Заказать звонок</span></button>
                    </div>
                </div>
                <div class="footer-block uslugi">
                    <h4>Услуги</h4>
                    <div class="links">
                        <ul>
                            <li>
                                <span>-</span><a href="visitka">Сайт визитка</a>
                            </li>
                            <li>
                                <span>-</span><a href="company">Бизнес сайт</a>
                            </li>
                            <li>
                                <span>-</span><a href="onlineShop">Интернет-магазин</a>
                            </li>
                            <li>
                                <span>-</span><a href="landing">Landing page</a>
                            </li>
                            <li>
                                <span>-</span><a class="our" href="seo">Продвижение</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="footer-block info">
                    <h4>Информация</h4>
                    <div class="links">
                        <ul>
                            <li>
                                <span>-</span><a href="visitka">Сайт визитка</a>
                            </li>
                            <li>
                                <span>-</span><a href="company">Бизнес сайт</a>
                            </li>
                            <li>
                                <span>-</span><a href="onlineShop">Интернет-магазин</a>
                            </li>
                            <li>
                                <span>-</span><a href="landing">Landing page</a>
                            </li>
                            <li>
                                <span>-</span><a href="seo">Продвижение</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="footer-block social">
                    <h4>Мы в соц. сетях</h4>
                    <div class="links">
                        <a href="https://vk.com/startgo" target="_blank">
                            <div class="link">
                                <div class="icon">
                                    <img src="" data-src="src/vk.png" alt="" class="lazy-load">
                                </div>
                                Вконтакте
                            </div>
                        </a>
                        <a href="https://www.instagram.com/startgo.pro/" target="_blank">
                            <div class="link">
                                <div class="icon">
                                    <img src="" data-src="src/insta.png" alt="" class="lazy-load">
                                </div>
                                Инстаграм
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </footer>`;
    }
    leftNavPortfolioInit(init = true) {
        let self = this;
        if(init) {
            import('nicescroll')
                .then(nicescroll => {
                    self.leftNavPortfolio = $(self.thisPageData).filter('.left-nav');
                    self.newPage.append(self.leftNavPortfolio);
                    NiceScroll.getjQuery('.infinite-block .wrap', {})
                    ('.infinite-block .wrap').niceScroll({
                        cursorcolor: '#ededed',
                        cursoropacitymax: 0.7,
                        scrollspeed: 100,
                        mousescrollstep: 80
                    });
                    $('.infinite-block .wrap').on('scroll', function() {
                        self.going = $(this).scrollTop();
                        if(self.going >= $(this)[0].scrollHeight - 500) {
                            $('.left-nav .scroll2.bottom').addClass('hidden');
                            $('.left-nav .gradient.bottom').addClass('hidden');
                        } else {
                            $('.left-nav .scroll2.bottom').removeClass('hidden');
                            $('.left-nav .gradient.bottom').removeClass('hidden');
                        }
                        if(self.going <= 0) {
                            $('.left-nav .scroll2.top').addClass('hidden');
                            $('.left-nav .gradient.top').addClass('hidden');
                        } else {
                            $('.left-nav .scroll2.top').removeClass('hidden');
                            $('.left-nav .gradient.top').removeClass('hidden');
                        }
                    });
                })
                .catch(e => console.error(e));
        } else {
            this.leftNavPortfolio = 0;
            $('.infinite-block .wrap').off('scroll');
        }
    }
    set(props) {
        this.props = Object.assign(this.props, props);
    }
    static closeAll() {
        this.isOpen = false;
        PageWalker.closeAllPages();
        PageWalker.offBodyScroll(false);
        $('.new-page>header .wrapper-gradient').removeClass('hide');
        //this.leftNavPortfolio = 0;
        $('.infinite-block .wrap').off('scroll');
        $('.new-page .tabs-container').off('click', '.tab a');
        return new Promise(resolve => {
            setTimeout(() => {
                $('.new-page').scrollTop(0);
                resolve();
            }, 750);
        });
    }
    static offBodyScroll(off = true) {
        if(off) {
            $('body').css({
                'overflow': 'hidden',
                'margin-right': '15px'
            });
        } else {
            $('body').css({
                'overflow': 'auto',
                'margin-right': '0'
            });
        }
    }
    static closeAllPages() {
        $('.layout, ' +
            '.new-page, ' +
            '.close-new-page, ' +
            '.close-new-page-portfolio, ' +
            '.modal, ' +
            '.modal .animate').removeClass('active');
    }
    static isUndefined(obj) {
        return typeof(obj) == 'undefined';
    }
    downLeftMenu() {
        this.going += 200;
        let niceScroll = NiceScroll.getjQuery('.infinite-block .wrap', {})
        ('.infinite-block .wrap').getNiceScroll(0);
        if(this.going >= niceScroll.getContentSize().h - 300) {
            $(this).addClass('hidden');
            return false;
        }
        niceScroll.doScrollTop(this.going, 0.35);
    }
    upLeftMenu() {
        this.going -= 200;
        let niceScroll = NiceScroll.getjQuery('.infinite-block .wrap', {})
        ('.infinite-block .wrap').getNiceScroll(0);
        niceScroll.doScrollTop(this.going, 0.35);
    }
}