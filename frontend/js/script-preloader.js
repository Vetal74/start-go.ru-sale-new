/**
 * Created by Виталий on 02.06.2017.
 */
import anime from 'animejs';
import ResLoader from './loader';

let preloader = document.querySelector('.preloader');

preloadStart();
setTimeout(() => {preloadComplete();}, 1600);

ResLoader.loadStyles('https://fonts.googleapis.com/css?family=Open+Sans:400,600,800&amp;subset=cyrillic', '', '');
ResLoader.loadStyles('critical');

document.querySelectorAll('.nbook path').forEach(el => strokeDash(el));

let slashAnimate = anime({
    targets: '.slash',
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'linear',
    duration: 300,
    delay: (el, i) => {
        if(el.classList.contains('para')) {
            return ((i - 1) * 120);
        } else {
            return (i * 120);
        }
    },
    opacity: {
        value: 0,
        easing: 'easeInQuart'
    },
    autoplay: false
});

let fillAnimate = anime({
    targets: '.tria',
    easing: 'linear',
    duration: '350',
    delay: function(el, i) {
        return 350 + (i * 200);
    },
    opacity: '1',
    autoplay: false
});

let mainSlideAnimate = anime({
    targets: '.nbook path',
    easing: 'linear',
    duration: '1000',
    strokeDashoffset: [anime.setDashoffset, 0],
    autoplay: false
});

export default function strokeDash(el) {
    const pathLength = el.getTotalLength();
    el.style.strokeDashoffset = pathLength;
}

function preloadStart() {
    preloader.classList.add('start');
    preloader.querySelectorAll('.slash').forEach(el => strokeDash(el));

    setTimeout(() => {
        slashAnimate.play();
        fillAnimate.play();
    }, 700);

    document.querySelector('body').style.overflow = 'hidden';
}
function preloadComplete() {
    import('./script')
        .then(res => {
            setTimeout(() => {
                document.querySelector('body').style.overflow = 'auto';
                preloader.classList.add('go');
            }, 200);
            setTimeout(() => {
                document.querySelector('body > .block-1').classList.add('active');
            }, 500);
            setTimeout(() => {
                mainSlideAnimate.play();
            }, 800);
        })
        .catch(e => console.error(e));
}


