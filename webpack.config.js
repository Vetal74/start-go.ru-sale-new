/**
 * Created by vetal on 21.04.17.
 */
// process.env.NODE_ENV ||
const NODE_ENV = 'development';
const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer');

let cssOptions = { importLoaders: 1 };
if(NODE_ENV !== 'development') cssOptions = { importLoaders: 1, minimize: true };
module.exports = {
    entry: {
        script: './frontend/js/script.js',
        style: './frontend/css/style.less',
        critical: './frontend/css/critical.less',
        scriptPreloader: './frontend/js/script-preloader.js',
        stylePreloader: './frontend/css/style-preloader.less'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: NODE_ENV == 'development' ?
            "/start-go.ru-sale-new/dist/" :
            "/sale-new/dist/",
        filename: '[name].js',
        chunkFilename: "[id].[name].js"
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        minimize: true
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [['es2015', {modules: false}]],
                        plugins: ['syntax-dynamic-import']
                    }
                }
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {loader: "css-loader", options: cssOptions },
                        {loader: "postcss-loader"}, "less-loader"
                    ]
                })
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                loaders: [
                    {
                        loader: "file-loader",
                        options: {
                            name: 'images/[name].[ext]'
                        }
                    }, {
                        loader: 'image-webpack-loader',
                        query: {
                            mozjpeg: {
                                progressive: true
                            },
                            gifsicle: {
                                interlaced: false
                            },
                            optipng: {
                                optimizationLevel: 4
                            },
                            pngquant: {
                                quality: '75-90',
                                speed: 3
                            }
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'css/[name].css',
            allChunks: true
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer()
                ]
            }
        }),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        })
    ],
    devtool: NODE_ENV == 'development' ? 'eval' : 'source-map',
    watch: NODE_ENV == 'development'
};

if (NODE_ENV != 'development') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: false,
                unsafe: true
            }
        })
    );
}
